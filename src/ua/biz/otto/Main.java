package ua.biz.otto;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Book[] books = new Book[11];

        String[] authors = {
                "Лев Толстой", "Марк Твен", "Николай Гоголь", "Эрих Мария Ремарк", "Антон Чехов", "Михаил Булгаков",
                "Артур Конан Дойл", "Александр Пушкин", "Михаил Шолохов", "Оскар Уайльд", "Александр Пушкин"
        };
        String[] titles = {
                "Анна Каренина", "Приключения Тома Сойера", "Мертвые души", "Три товарища", "Вишневый сад",
                "Мастер и Маргарита", "Приключения Шерлока Холмса", "Евгений Онегин", "Тихий Дон", "Портрет Дориана Грея",
                "Капитанская дочка"
        };

        int[] years = {1878, 1876, 1842, 1970, 1904, 1967, 1892, 1833, 1940, 1890, 1836};


        for (int i = 0; i < books.length; i++) {
            Book book = new Book();
            book.author = authors[i];
            book.title = titles[i];
            book.year = years[i];
            books[i] = book;
        }

        for (Book book : books) {
            System.out.println("***");
            System.out.println(book.author);
            System.out.println(book.title);
            System.out.println(book.year);

        }
        System.out.println("\n");


        // Find oldest book
        int oldestBook = books[0].year;
        String oldestBookAuthor = books[0].author;
        String oldestBookTitle = books[0].title;
        for (int i = 0; i < books.length; i++) {
            if (books[i].year < oldestBook) {
                oldestBook = books[i].year;
                oldestBookAuthor = books[i].author;
                oldestBookTitle = books[i].title;
            }

        }
        System.out.println("Oldest book author is: " + oldestBookAuthor + " " + oldestBookTitle + " " + oldestBook);

        // Find certain author
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input author's first or last name: ");
        String author = scanner.nextLine().toLowerCase();
        for (int i = 0; i < books.length; i++) {
            if (books[i].author.toLowerCase().contains(author))
                System.out.println("Author's book is : " + books[i].author + " " + books[i].title);
        }

        // Find previous years
        System.out.println("Input year: ");
        int year = scanner.nextInt();
        for (int i = 0; i < books.length; i++) {
            if (books[i].year <= year) {
                System.out.println("Previous years are: " + books[i].year + " " + books[i].author + " " + books[i].title);
            }
        }
        System.out.println("\n");


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //// Currency exchange

        //Массивы курсов валют
        float[] alfaBank = {26.60f, 32.95f, 0.47f};
        float[] raiffeisenBankAval = {26.60f, 32.95f, 0.46f};
        float[] otpBank = {26.50f, 32.80f, 0.47f};
        float[] universalBank = {26.46f, 32.75f, 0.46f};
        float[] privatBank = {26.55f, 32.80f, 0.47f};
        float[] pumbBank = {26.70f, 32.90f, 0.46f};

        // Enter currency
        // Номер и название валюты соответствует позиции в массиве
        System.out.println("Choose currency: \n" +
                "0. USD\n" +
                "1. EUR\n" +
                "2. RUB");
        int currency = scanner.nextInt();

        //Enter amount of money
        System.out.println("Enter the amount of UAH to exchange: ");
        int amount = scanner.nextInt();

        // Choose bank
        System.out.println("Choose bank: \n" +
                "0. alfaBank;\n" +
                "1. raiffeisenBankAval;\n" +
                "2. otpBank;\n" +
                "3. universalBank;\n" +
                "4. privatBank;\n" +
                "5. pumbBank");
        int bank = scanner.nextInt();

        switch (bank) {
            case 0:
                System.out.println(amount / alfaBank[currency]);
                break;
            case 1:
                System.out.println(amount / raiffeisenBankAval[currency]);
                break;
            case 2:
                System.out.println(amount / otpBank[currency]);
                break;
            case 3:
                System.out.println(amount / universalBank[currency]);
                break;
            case 4:
                System.out.println(amount / privatBank[currency]);
                break;
            case 5:
                System.out.println(amount / pumbBank[currency]);
                break;
        }
    }
}
